package org.sid.servicecompany.dao;

import org.sid.servicecompany.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;


@Component
public interface CompanyRepository extends JpaRepository<Company,Long> {
}
